const express = require('express')
const fs = require('fs')
const path = require('path')
const multer = require('multer')
const router = express.Router()

const storage = multer.diskStorage({
	destination: function(req, file, cb) {
		cb(null, './uploads/')
	},
	filename: function(req, file, cb) {
		cb(null, file.originalname)
	}
})

const upload = multer({ storage })

const barbecue_controller = require('../controllers/barbecue.controller')

router.get('/get/:userId', barbecue_controller.getBarbecuesByUser)
router.post('/location', barbecue_controller.getBarbecuesByLocation)
router.post('/create', upload.single('image'), barbecue_controller.createBarbecue)
router.put('/update', barbecue_controller.updateBarbecue)
router.post('/times', barbecue_controller.getBookingByDate)
router.put('/bookings', barbecue_controller.updateBookings)
router.get('/bookings/:id', barbecue_controller.getBookings)
router.delete('/delete/:id', barbecue_controller.deleteBarbecue)

module.exports = router