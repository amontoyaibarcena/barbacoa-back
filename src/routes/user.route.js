const express = require('express')
const router = express.Router()

const user_controller = require('../controllers/user.controller')

router.get('/get/:id', user_controller.getUser)
router.post('/create', user_controller.createUser)
router.post('/validate', user_controller.validateUser)

module.exports = router