const mongoose = require('mongoose')
const Schema = mongoose.Schema

const BarbecueSchema = new Schema({
  model: { type: String, require: true },
  description: { type: String, require: true },
  zipcode: { type: String, require: true },
  address: {type: String, require: true },
  path: { type: String, require: true },
  bookings: [{
    date: { type: String, require: true },
    detail: [{
      userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        require: true
      },
      range: { type: [ Number ], require: true }
    }]
  }],
  userId: { 
  	type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    require: true
	}
}, { versionKey: false })

module.exports = mongoose.model('Barbecue', BarbecueSchema)