const Barbecue = require('../models/barbecue.model')
const googleMapsClient = require('@google/maps').createClient({
  key: 'AIzaSyBfj26goqLtbqLq8lm_Z_YuxEvJ9nz335E'
});

exports.getBarbecuesByUser = (req, res) => {
  Barbecue.find({userId: req.params.userId}, (err, barbecues) => {
    if (err) {
      res.status(500).send(err);
    }
    res.status(200).json(barbecues);
  })
}

exports.getBarbecuesByLocation = async (req, res) => {
	let nearBarbecues = []
	const allBarbecues = await Barbecue.find().exec()
	const destinations = allBarbecues.map(barbecue => {
		return barbecue.zipcode
	})
	googleMapsClient.distanceMatrix({
    origins: [`${req.body.latitude},${req.body.longitude}`],
    destinations: destinations
  }, function(err, response) {
	  if (!err && response.json.status === 'OK') {
	    response.json.rows[0].elements.map((element, index) => {
	    	if(element.distance.value <= 10000){
	    		nearBarbecues.push(allBarbecues[index])
	    	}
	    })
	    res.send({success: true, data: nearBarbecues})
	  } else {
	  	res.send({success: false, data: null})
	  }
	})
}

exports.createBarbecue = (req, res) => {
  const barbecue = new Barbecue({
		...req.body,
		path: req.file.path,
    bookings: []
	})
	barbecue.zipcode = barbecue.zipcode + ', Peru'
	barbecue
		.save()
		.then(result => {
			res.send({success: true, barbecue: result })
		})
}

exports.updateBarbecue = async function (req, res) {
  Barbecue.findOneAndUpdate(
    { _id: req.body._id },
    {model: req.body.model, description: req.body.description},
    { new: true },
    (err, barbecue) => {
      if (err) {
        res.status(500).send(err)
      }
      res.status(200).json(barbecue)
    }
  )
}         

exports.updateBookings = async function (req, res) {
  let barbecue = await Barbecue.findOne({ _id: req.body.id }).exec()
  let range = []
  for(let i = req.body.startTime; i <= req.body.endTime; i++){
    range.push(i)
  }
  const index = barbecue.bookings.findIndex(booking => booking.date === req.body.date)
  if(index !== -1){
    barbecue.bookings[index].detail.push({
      userId: req.body.userId,
      range
    })
  } else {
    barbecue.bookings.push({
      date: req.body.date,
      detail: [{
        userId: req.body.userId,
        range
      }]
    })
  }
  const barbecueObj = await barbecue.save() 
  res.send(barbecueObj)
}           

exports.getBookingByDate = async (req, res) => {
  let barbecue = await Barbecue.findOne({ _id: req.body.id }).exec()
  const booking = barbecue.bookings.find(booking => booking.date === req.body.date)
  let times = []
  if(booking !== undefined){
    booking.detail.forEach(detail => {
      times = times.concat(detail.range)
    })
  }
  res.send([...new Set(times)])
}            

exports.getBookings = async (req, res) => {
  const barbecueObj = await Barbecue.findOne({ _id: req.params.id })
    .select('bookings')
    .populate('bookings.detail.userId', 'firstName lastName')
    .exec((err, barbecue) => {
    if (err) {
      res.status(500).send(err);
    }
    res.status(200).json(barbecue);
  })
  
}                           

exports.deleteBarbecue = (req, res) => {
  Barbecue.remove({ _id: req.params.id }, (err, barbecue) => {
    if (err) {
      res.status(404).send(err);
    }
    res.status(200).json({ message: "Institution successfully deleted" });
  })
}

