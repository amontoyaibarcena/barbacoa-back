const User = require('../models/user.model')
const CryptoJS = require("crypto-js")

exports.getUser = (req, res) => {
  User.findOne({_id: req.params.id}, { password: 0 }, (err, user) => {
    if (err) {
      res.status(500).send(err)
    }
    res.status(200).json(user)
  })
}

exports.validateUser = async function (req, res) {
  let userObj
  if (req.body.email) {
    userObj = await User.findOne({email: req.body.email}, {password: 1}).exec()
  } else {
    userObj = await User.findOne({_id: req.body.id}, {password: 1}).exec()
  }
  let response = {
    success: false,
    user: null
  }
  if (userObj) {
    const bytes1  = CryptoJS.AES.decrypt(req.body.password, 'key')
    const plaintext1 = bytes1.toString(CryptoJS.enc.Utf8)
    const bytes2  = CryptoJS.AES.decrypt(userObj.password, 'key')
    const plaintext2 = bytes2.toString(CryptoJS.enc.Utf8)
    if (plaintext1 === plaintext2) {
      const user = await User.findOne({email: req.body.email}, {userId: 1, access: 1}).exec()
      response.success = true
      response.user = user
    }
  }
  res.send(JSON.stringify(response))
}

exports.createUser = function (req, res) {
  let newUser = new User(req.body)
  newUser.save((err, user) => {
    if (err) {
      res.status(500).send(err)
    }
    res.status(201).json(user)
  })
}