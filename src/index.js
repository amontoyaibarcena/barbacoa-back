let express = require('express')
let bodyParser = require("body-parser")
let cors = require('cors')
let path = require('path')
let app = express()

//Allowing cors
app.use(cors({
	origin: '*',
 	methods: 'GET,PUT,POST,DELETE',
}))

//Routes
const user = require('./routes/user.route')
const barbecue = require('./routes/barbecue.route')

//DB connection
require("./config/db")

app.use(bodyParser.json({limit: '50mb'}))
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }))
app.use('/uploads', express.static('uploads'));

//API endpoints
app.use('/users', user)
app.use('/barbecues', barbecue)


const PORT = process.env.PORT || 3001


//Start server on port PORT
app.listen(PORT, () => {
	console.info(`Server has started on ${PORT}`)
})
